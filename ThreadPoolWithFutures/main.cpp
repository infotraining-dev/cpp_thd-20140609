#include <iostream>
#include <thread>
#include <future>
#include <chrono>
#include <functional>
#include <memory>
#include <type_traits>

#include "thread_safe_queue.hpp"

using namespace std;

typedef std::function<void()> Task;

class ThreadPool
{
public:
    ThreadPool(size_t no_of_threads) : no_of_threads_(no_of_threads)
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            threads_.emplace_back([this] { run(); });
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    ~ThreadPool()
    {
        for(size_t i = 0; i < no_of_threads_; ++i)
            task_queue_.push(END_OF_WORK);

        for(auto& t : threads_)
            t.join();
    }

    template <typename F>
    std::future<typename std::result_of<F()>::type> submit(F fun)
    {
        typedef typename std::result_of<F()>::type Result;

        auto task = std::make_shared<std::packaged_task<Result()>>(fun);
        std::future<Result> fresult = task->get_future();
        task_queue_.push([task]() { (*task)(); });

        return fresult;
    }

private:
    static const Task END_OF_WORK;

    size_t no_of_threads_;
    ThreadSafeQueue<Task> task_queue_;
    std::vector<std::thread> threads_;

    void run()
    {
        while (true)
        {
            Task task;
            task_queue_.wait_and_pop(task);

            if (!task)
                return;

            task();
        }
    }
};

const Task ThreadPool::END_OF_WORK;

void do_something(const string& data)
{
    cout << "Start: " << data << endl;
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
    cout << "Done: " << data << endl;
}

int calc_something(int data)
{
    cout << "Start: " << data << endl;
    boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));
    return data * data;
}

int main()
{
    ThreadPool thread_pool(5);

    for(int i = 0; i < 10; ++i)
    {
        thread_pool.submit([=] { do_something(to_string(i)); });
    }

    vector<std::future<int>> results;

    for(int i = 0; i < 10; ++i)
        results.push_back(
                    thread_pool.submit([=] { return calc_something(i); }));

    cout << "Results: ";

    for(auto& r : results)
    {
        cout << r.get() << endl;
    }

    cout << endl;
}
