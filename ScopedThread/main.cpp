#include <iostream>
#include <thread>
#include <chrono>
#include <stdexcept>

using namespace std;

class ThreadJoinGuard
{
    thread& thd_;
public:
    ThreadJoinGuard(thread& thd) : thd_(thd) {}

    ThreadJoinGuard(const ThreadJoinGuard&) = delete;
    ThreadJoinGuard& operator=(const ThreadJoinGuard&) = delete;

    ~ThreadJoinGuard()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

class ScopedThread
{
    thread thd_;
public:
    ScopedThread(thread&& thd) : thd_(move(thd)) {}

    template <typename... T>
    ScopedThread(T&&... args) : thd_(forward<T>(args)...)
    {
    }

    ScopedThread(const ScopedThread&) = delete;
    ScopedThread& operator=(const ScopedThread&) = delete;

    void join()
    {
        thd_.join();
    }

    ~ScopedThread()
    {
        if (thd_.joinable())
            thd_.join();
    }
};


void background_work(int id, size_t ms)
{
    for(int i = 0; i < 10; ++i)
    {
        std::cout << "THD#" << id << ": " << i << std::endl;

        this_thread::sleep_for(chrono::milliseconds(ms));
    }
}

void hello()
{
    std::string text = "Hello Concurrent World";

    for(auto c : text)
    {
        std::cout << c << " ";
        this_thread::sleep_for(chrono::milliseconds(100));
        std::cout.flush();
    }

    std::cout << std::endl;
}

void may_throw(int arg)
{
    if (arg % 10 == 0)
        throw runtime_error("Error");
}

void local_scope()
{
    // 1-szy sposób
    //thread thd(&background_work, 1, 100);
    //ThreadJoinGuard thd_guard(thd);

    // 2-gi sposób
    //ScopedThread scoped_thd(thread(&background_work, 1, 100));
    ScopedThread scoped_thd1(background_work, 1, 100);
    ScopedThread scoped_thd2(hello);

    for(int i = 0; i < 100; ++i)
    {
        may_throw(i);
    }
}

int main()
{
    try
    {
        local_scope();
    }
    catch(const runtime_error& e)
    {
        cout << e.what() << endl;
    }
}
