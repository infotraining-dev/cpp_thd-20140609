#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>

template <typename It, typename T>
void partial_accumulate(It first, It last, T& result)
{
    result = std::accumulate(first, last, T());
}

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned hardware_threads_count = std::thread::hardware_concurrency();

    if (hardware_threads_count == 0)
        hardware_threads_count = 1;

    std::vector<std::thread> thds(hardware_threads_count-1);
    std::vector<T> partial_results(hardware_threads_count);

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_threads_count;

    It range_start = first;
    for(unsigned i = 0; i < hardware_threads_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        thds[i] = std::thread(&partial_accumulate<It, T>, range_start, range_end,
                               std::ref(partial_results[i]));
        range_start = range_end;
    }

    partial_accumulate(range_start, last, partial_results[hardware_threads_count-1]);

    for(auto& t : thds)
        t.join();

    return std::accumulate(partial_results.begin(), partial_results.end(), init);
}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v;
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    return 0;
}
