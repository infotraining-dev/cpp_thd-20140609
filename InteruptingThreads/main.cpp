#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>

using namespace std;

void background_work(int id, size_t ms)
{
    try
    {
        int i = 0;
        while(true)
        {
            ++i;
            {
                //boost::this_thread::disable_interruption ds;

                std::cout << "Step1 THD#" << id << ": " << i << std::endl;

                boost::this_thread::interruption_point();

                std::cout << "Step2 THD#" << id << ": " << i << std::endl;

                boost::this_thread::interruption_point();

                //boost::this_thread::restore_interruption ri(ds);
            }

            std::cout << "Step3 THD#" << id << ": " << i << std::endl;

            boost::this_thread::interruption_point();
        }
    }
    catch(const boost::thread_interrupted& e)
    {
        cout << "Przerwano wykonanie wątku#" << id << endl;
    }
}

int main()
{
    boost::thread thd0;

    auto id0 = thd0.get_id();

    cout << "THD0 - " << id0 << endl;

    boost::thread thd1(&background_work, 1, 300);

    auto id1 = thd1.get_id();

    cout << "THD1 - " << id1 << endl;

    thd0 = std::move(thd1);

    cout << "After move:\n";

    cout << "THD0 - " << thd0.get_id() << endl;
    cout << "THD1 - " << thd1.get_id() << endl;

    boost::this_thread::sleep_for(boost::chrono::milliseconds(5));

    thd0.interrupt();

    thd0.join();
}
