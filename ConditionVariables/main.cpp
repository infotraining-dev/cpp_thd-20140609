#include <iostream>
#include <vector>
#include <algorithm>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

class Data
{
    vector<int> data_;
    bool is_ready_ = false;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    void read()
    {
        cout << "Start reading..." << endl;
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] { return rand() % 100; });
        std::unique_lock<std::mutex> lk(mtx_);
        is_ready_ = true;
        cond_.notify_all();
        lk.unlock();
        cout << "End reading..." << endl;
    }

    void process(int id)
    {
        std::unique_lock<std::mutex> lk(mtx_);

        cond_.wait(lk, [this] { return is_ready_; });

        int sum = accumulate(data_.begin(), data_.end(), 0);

        lk.unlock();

        cout << "Id: " << id << " Sum: " << sum << endl;
    }
};

int main()
{
    Data data;

    std::thread thd1(&Data::process, ref(data), 1);
    std::thread thd2(&Data::process, ref(data), 2);
    std::thread thd3(&Data::read, ref(data));

    thd1.join();
    thd2.join();
    thd3.join();
}
