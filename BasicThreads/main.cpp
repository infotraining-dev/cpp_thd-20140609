#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <thread>
#include <chrono>
#include <vector>
#include <functional>
#include <cassert>

namespace impl = boost;

void hello()
{
    std::string text = "Hello Concurrent World";

    for(auto c : text)
    {
        std::cout << c << " ";
        impl::this_thread::sleep_for(impl::chrono::milliseconds(100));
        std::cout.flush();
    }

    std::cout << std::endl;
}

void background_work(int id, size_t ms)
{
    for(int i = 0; i < 10; ++i)
    {
        std::cout << "THD#" << id << ": " << i << std::endl;

        impl::this_thread::sleep_for(impl::chrono::milliseconds(ms));
    }
}

class BackgroundWork
{
    const int id_;
public:
    BackgroundWork(int id) : id_(id) {}

    void operator()(int ms)
    {
        for(int i = 0; i < 10; ++i)
        {
            std::cout << "BW#" << id_ << ": " << i << std::endl;

            impl::this_thread::sleep_for(impl::chrono::milliseconds(ms));
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};

class Buffer
{
    std::vector<int> buffer_;
public:
    void assign(const std::vector<int>& source)
    {
        buffer_.assign(source.begin(), source.end());
    }

    std::vector<int> data() const
    {
        return buffer_;
    }
};

template <typename Container>
void print(const Container& c, const std::string& prefix)
{
    std::cout << prefix << ": [ ";
    for(const auto& item : c)
    {
        std::cout << item << " ";
    }

    std::cout << "]" << std::endl;
}

void run_as_deamon()
{
    impl::thread thd(BackgroundWork(99), 2000);

    thd.detach();

    if (!thd.joinable())
        std::cout << "Wątek thd jest demonem" << std::endl;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    run_as_deamon();

    impl::thread thd1(&hello);
    impl::thread thd2(&hello);

    hello();

    thd1.join();
    thd2.join();

    std::cout << "\n-------------\n\n";

    impl::thread thd3(&background_work, 1, 100);
    impl::thread thd4(&background_work, 2, 200);

    BackgroundWork bw1(1);
    impl::thread thd5(bw1, 200);
    impl::thread thd6(BackgroundWork(2), 300);

    thd3.join();
    thd4.join();
    thd5.join();
    thd6.join();

    std::cout << "\n-------------\n\n";

    std::vector<int> data = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    Buffer buff1;
    Buffer buff2;

    impl::thread thd7(
                std::bind(&Buffer::assign,
                            std::ref(buff1),
                            std::cref(data)));

    impl::thread thd8([&]{ buff2.assign(data); });

    impl::thread thd9 = std::move(thd8);

    thd8 = std::move(thd7);

    thd8.join();
    thd9.join();

    print(buff1.data(), "buff1");
    print(buff2.data(), "buff2");

    std::cout << "Main thread ends..." << std::endl;
}
