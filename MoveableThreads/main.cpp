#include <iostream>
#include <thread>
#include <vector>
#include <functional>

using namespace std;

void background_work(int id, size_t ms)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;

        this_thread::sleep_for(chrono::milliseconds(ms));
    }

    cout << "End of work THD#: " << id << endl;
}

std::thread create_thread(int id)
{
    std::thread thd(&background_work, id, 200);

    return thd;
}

int main()
{
    thread thd1 = create_thread(1);
    thread thd2 = create_thread(2);

    vector<thread> threads(2);

    threads[0] = move(thd1);
    threads[1] = move(thd2);

    thread thd3(&background_work, 3, 150);

    threads.push_back(move(thd3));
    threads.push_back(thread(&background_work, 4, 300));
    threads.push_back(thread(&background_work, 5, 100));

    for(auto& thd : threads)
        thd.join();
}

