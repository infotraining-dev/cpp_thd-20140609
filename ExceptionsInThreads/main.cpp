#include <iostream>
#include <thread>
#include <vector>
#include <stdexcept>

using namespace std;

void may_throw(int id, int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13 from THD#" + to_string(id));
}

void background_work(int id, size_t count, exception_ptr& excpt)
{
    try
    {
        for(int i = 0; i < count; ++i)
        {
            std::cout << "THD#" << id << ": " << i << std::endl;

            this_thread::sleep_for(chrono::milliseconds(100));

            may_throw(id, i);
        }

        cout << "THD#" << id << " has finished...\n";
    }
    catch(...)
    {
        cout << "Catching exception..." << endl;

        excpt = current_exception();
    }
}

int main()
{
    cout << "Hardware concurrency: " << thread::hardware_concurrency() << endl;

    const int NO_OF_THREADS = 4;

    vector<thread> threads(NO_OF_THREADS);
    vector<exception_ptr> excpts(NO_OF_THREADS);

    for(int i = 0; i < NO_OF_THREADS; ++i)
        threads[i] = thread(&background_work, i, (i + 1) * 10, ref(excpts[i]));

    for(auto& thd : threads)
        thd.join();

    for(auto& e : excpts)
        if (e)
        {
            try
            {
                rethrow_exception(e);
            }
            catch(const runtime_error& e)
            {
                cout << "Main catch: " << e.what() << endl;
            }
        }
}
