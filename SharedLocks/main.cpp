#include <iostream>
#include <unordered_map>
#include <boost/thread.hpp>

using namespace std;

typedef string HostName;
typedef string Ip;

class Dns
{
    std::unordered_map<HostName, Ip> dns_;
    mutable boost::shared_mutex mtx_;
public:
    Ip get_ip(const HostName& host_name) const
    {
        cout << "Start get_ip: " << host_name << endl;

        boost::shared_lock<boost::shared_mutex> lk(mtx_);

        auto ip = dns_.find(host_name);

        boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));

        if (ip != dns_.end())
        {
            cout << "End get_ip: " << host_name << endl;
            return ip->second;
        }

        throw runtime_error("Host " + host_name + " not found");
    }

    void update(const HostName& host_name, const Ip& ip)
    {
        cout << "Start update: " << host_name << endl;

        boost::lock_guard<boost::shared_mutex> lk(mtx_);

        dns_.insert(make_pair(host_name, ip));

        boost::this_thread::sleep_for(boost::chrono::milliseconds(2000));

        cout << "End update: " << host_name << endl;
    }
};

void read(const Dns& dns, const HostName& name)
{
    cout << "Reading " << name << "..." << endl;
    cout << name << " : " << dns.get_ip(name) << endl;
}

void write(Dns& dns, const HostName& name, const Ip& ip )
{
    cout << "Updating: " << name << " : " << ip << endl;
    dns.update(name, ip);
}

int main()
{
    Dns dns;

    dns.update("google.pl", "123.44.44.33");
    dns.update("infotraining.pl", "43.4.14.31");
    dns.update("nokia.com", "33.55.33.33");

    cout << "\n\n--------------------------\n\n";

    vector<HostName> names = { "google.pl", "infotraining.pl", "nokia.com" };

    boost::thread_group thd_group;

    //thd_group.create_thread([&] { write(dns, "google.pl", "66.66.66.66");});

    for(const auto& name : names)
        thd_group.create_thread([&] { read(dns, name); });

    thd_group.join_all();
}
