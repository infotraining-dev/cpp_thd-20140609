#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <atomic>

using namespace std;

int counter = 0;

boost::mutex mtx;

void worker()
{
    for (int i = 0 ; i < 10000000 ; ++i)
    {
        boost::lock_guard<boost::mutex> lk(mtx);
        counter++;
    }
}

std::atomic<int> atomic_counter;

void worker_with_atomics()
{
    for (int i = 0 ; i < 10000000 ; ++i)
        atomic_counter++;
}

int main()
{
    auto start = boost::chrono::high_resolution_clock::now();
    boost::thread th1(worker);
    boost::thread th2(worker);
    th1.join();
    th2.join();
    auto end = boost::chrono::high_resolution_clock::now();
    float seconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(end-start).count();
    std::cout << "Time: " << seconds << " msec" << std::endl;

    cout << "Counter = " << counter << endl;


    start = boost::chrono::high_resolution_clock::now();
    boost::thread th3(worker_with_atomics);
    boost::thread th4(worker_with_atomics);
    th3.join();
    th4.join();
    end = boost::chrono::high_resolution_clock::now();
    seconds = boost::chrono::duration_cast<boost::chrono::milliseconds>(end-start).count();
    std::cout << "Time: " << seconds << " msec" << std::endl;

    cout << "Counter = " << counter << endl;
}

