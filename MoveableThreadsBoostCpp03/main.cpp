#include <iostream>
#include <boost/thread.hpp>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include <vector>
#include <functional>

using namespace std;

void background_work(int id, size_t ms)
{
    for(int i = 0; i < 10; ++i)
    {
        cout << "THD#" << id << ": " << i << endl;

        boost::this_thread::sleep_for(boost::chrono::milliseconds(ms));
    }

    cout << "End of work THD#: " << id << endl;
}

boost::thread create_thread(int id)
{
    boost::thread thd(&background_work, id, 200);

    return boost::move(thd);
}

int main()
{
    boost::thread thd1 = create_thread(1);
    boost::thread thd2 = create_thread(2);

    thd1.join();
    thd2.join();

    vector<boost::thread> threads(2);

    threads[0] = move(thd1);
    threads[1] = move(thd2);

    boost::thread thd3(&background_work, 3, 150);

    threads.push_back(move(thd3));
    threads.push_back(boost::thread(&background_work, 4, 300));
    threads.push_back(boost::thread(&background_work, 5, 100));

    for(vector<boost::thread>::iterator it = threads.begin(); it != threads.end(); ++it)
        it->join();

    // Stara wersja biblioteki Boost < 1.50
    cout << "\nThreadGroup -------------\n";

    boost::thread_group group_thd;

    group_thd.create_thread(boost::bind(&background_work, 6, 100));
    group_thd.create_thread(boost::bind(&background_work, 7, 200));

    group_thd.add_thread(new boost::thread(&background_work, 8, 150));

    group_thd.join_all();
}

