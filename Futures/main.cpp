#include <iostream>
#include <future>
#include <chrono>
#include <functional>
#include <stdexcept>
#include <vector>

using namespace std;

void f(const string& name)
{
    cout << "Start: " << name << endl;
    this_thread::sleep_for(chrono::milliseconds(2000));
    cout << "End: " << name << endl;
}

int calculate_square(int x)
{
    cout << "Starting calculation for " << x << endl;

    int interval = rand() % 5000;

    this_thread::sleep_for(chrono::milliseconds(interval));

    if (x == 13)
        throw runtime_error("Error#13");

    return x * x;
}

void may_throw(int arg)
{
    if (arg == 13)
        throw runtime_error("Error#13");
}

class SquareCalculator
{
    std::promise<int> promise_;
public:
    void operator()(int x)
    {
        int interval = rand() % 5000;

        this_thread::sleep_for(chrono::milliseconds(interval));

        try
        {
            may_throw(x);
        }
        catch(...)
        {
            exception_ptr ex = current_exception();
            promise_.set_exception(ex);
            return;
        }

        promise_.set_value(x * x);
    }

    std::future<int> get_future()
    {
        return promise_.get_future();
    }
};

int main()
{
    using namespace placeholders;

    // 1 - szy sposob
    std::packaged_task<int()> pt1([] { return calculate_square(12); });
    std::packaged_task<int()> pt2(bind(&calculate_square, 9));
    std::packaged_task<int(int)> pt3(&calculate_square);
    std::packaged_task<void(string)> pt4(&f);

    std::future<int> f1 = pt1.get_future();
    std::future<int> f2 = pt2.get_future();
    std::future<int> f3 = pt3.get_future();
    std::future<void> f4 = pt4.get_future();

    std::thread thd1(move(pt1));
    std::thread thd2(move(pt2));
    std::thread thd3(move(pt3), 13);
    std::thread thd4(move(pt4), "Async task");

    try
    {
        cout << "f1 = " << f1.get() << endl;
        cout << "f2 = " << f2.get() << endl;
        cout << "f3 = " << f3.get() << endl;
        f4.wait();
    }
    catch(const runtime_error& e)
    {
        cout << e.what() << endl;
    }

    thd1.join();
    thd2.join();
    thd3.join();
    thd4.join();

    // 2-gi sposob
    SquareCalculator square_calc;

    std::future<int> f5 = square_calc.get_future();

    std::thread thd5(ref(square_calc), 29);

    cout << "f5 = " << f5.get() << endl;
    thd5.join();

    // 3. sposob
    vector<future<int>> future_squares;

    for(int i = 1; i <= 10; ++i)
        future_squares.push_back(async(launch::async, &calculate_square, i));

    cout << "Starting async..." << endl;
    this_thread::sleep_for(chrono::milliseconds(3000));

    for(auto& f : future_squares)
        cout << "Result: " << f.get() << endl;
}
