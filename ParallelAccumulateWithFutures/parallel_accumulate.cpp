#include <iostream>
#include <thread>
#include <vector>
#include <list>
#include <algorithm>
#include <chrono>
#include <future>

template <typename It, typename T>
T parallel_accumulate(It first, It last, T init)
{
    unsigned hardware_threads_count = std::thread::hardware_concurrency();

    if (hardware_threads_count == 0)
        hardware_threads_count = 1;

    std::vector<std::future<T>> partial_results(hardware_threads_count - 1);

    size_t count = std::distance(first, last);
    size_t range_size = count / hardware_threads_count;

    It range_start = first;
    for(unsigned i = 0; i < hardware_threads_count - 1; ++i)
    {
        It range_end = range_start;
        std::advance(range_end, range_size);

        partial_results[i] =
                std::async(std::launch::async,
                           &std::accumulate<It, T>, range_start, range_end, T());

        range_start = range_end;
    }

    init += std::accumulate(range_start, last, T());

    return std::accumulate(partial_results.begin(), partial_results.end(),
                           init, [](T v, std::future<T>& f) { return v + f.get(); });
}


int main(int argc, char *argv[])
{
    const size_t SIZE = 50000000;

    typedef unsigned long long int ValueType;

    std::vector<ValueType> v;
    for (size_t i = 0 ; i < SIZE; ++i)
    {
        v.push_back(i);
    }

    {
        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     std::accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    {
        std::cout << "\nParallel\n";

        auto start = std::chrono::high_resolution_clock::now();
        std::cout << "Accumulate:          " <<
                     parallel_accumulate(v.begin(), v.end(), (ValueType)0);
        auto end = std::chrono::high_resolution_clock::now();
        float seconds = std::chrono::duration_cast<std::chrono::milliseconds>(end-start).count();
        std::cout << "   " << seconds << " ms" << std::endl;
    }

    return 0;
}
