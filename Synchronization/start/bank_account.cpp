#include <thread>
#include <iostream>
#include <mutex>
#include <boost/thread.hpp>
#include <boost/thread/externally_locked.hpp>

class BankAccount
{
    typedef std::recursive_mutex Mutex;

    const int id_;
    double balance_;
    mutable Mutex mtx_;
public:
    BankAccount(int id, double balance) : id_(id), balance_(balance)
    {
    }

    void print()
    {
        std::cout << "Bank Account " << id_ << std::endl;
        std::lock_guard<Mutex> lk(mtx_);
        std::cout << "balance = " << balance_ << std::endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        std::unique_lock<Mutex> lk_from(mtx_, std::defer_lock);
        std::unique_lock<Mutex> lk_to(to.mtx_, std::defer_lock);

        std::lock(lk_from, lk_to);

        withdraw(amount);
        to.deposit(amount);

//        balance_ -= amount;
//        to.balance_ += amount;
    }

    void withdraw(double amount)
    {
        std::lock_guard<Mutex> lk(mtx_);
        balance_ -= amount;
    }

    void deposit(double amount)
    {
        std::lock_guard<Mutex> lk(mtx_);
        balance_ += amount;
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard<Mutex> lk(mtx_);
        return balance_;
    }
};


void make_withdraws(BankAccount& ba, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        ba.withdraw(1.0);
}

void make_deposits(BankAccount& ba, int no_of_operations)
{
    for(int i = 0; i < no_of_operations; ++i)
        ba.deposit(1.0);
}

void make_transfers(BankAccount& from, BankAccount& to, int no_of_operations, int thd_id)
{
    for(int i = 0; i < no_of_operations; ++i)
    {
        std::cout << "THD#" << thd_id << " transfer from ba#" << from.id()
                  << " to ba#" << to.id() << std::endl;

        from.transfer(to, 1.0);
    }
}

int main()
{
    const int NO_OF_ITERS = 1000000;

    BankAccount ba1(1, 10000);
    BankAccount ba2(2, 10000);

    std::thread thd1(&make_withdraws, std::ref(ba1), NO_OF_ITERS);
    std::thread thd2(&make_deposits, std::ref(ba1), NO_OF_ITERS);

    thd1.join();
    thd2.join();

    std::cout << "After threads: ";
    ba1.print();

    std::cout << "\nTransfer:" << std::endl;

    std::thread thd3(&make_transfers, std::ref(ba1), std::ref(ba2), NO_OF_ITERS, 1);
    std::thread thd4(&make_transfers, std::ref(ba2), std::ref(ba1), NO_OF_ITERS, 2);

    thd3.join();
    thd4.join();

    ba1.print();
    ba2.print();

    // transakcja
//    BankAccount ba3(3, 1000.0);

//    {
//        std::lock_guard<BankAccount> lk(ba3);

//        ba3.withdraw(100);
//        ba3.deposit(300);
//    }
}
