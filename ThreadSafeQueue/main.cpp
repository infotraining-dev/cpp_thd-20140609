#include <iostream>
#include <queue>
#include <mutex>
#include <thread>
#include <condition_variable>
#include <algorithm>
#include <boost/algorithm/string.hpp>

#include "thread_safe_queue.hpp"

using namespace std;

typedef string DataType;

class ProducerConsumer
{
    ThreadSafeQueue<DataType> queue_;

public:
    void produce(const DataType& data, int consumer_count)
    {
        DataType item = data;

        while(next_permutation(item.begin(), item.end()))
        {
            cout << "Producing: " << item << endl;
            queue_.push(item);
            std::this_thread::sleep_for(std::chrono::milliseconds(50));
        }

        // end of job
        for(int i = 0; i <  consumer_count; ++i)
            queue_.push("");
    }

    void consumer(int id)
    {
        while (true)
        {
            DataType item;
            queue_.wait_and_pop(item);

            if (item != "")
            {
                cout << "Consumer#" << id << " processed an item: "
                     << process_data(item) << endl;
            }
            else
            {
                cout << "End of job - consumer#" << id << endl;
                return;
            }
        }
    }

    DataType process_data(const DataType& data)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
        return boost::to_upper_copy(data);
    }
};

int main()
{
    ProducerConsumer pc;

    std::thread thd1(&ProducerConsumer::produce, ref(pc), "abcd", 2);
    std::thread thd2(&ProducerConsumer::consumer, ref(pc), 1);
    std::thread thd3(&ProducerConsumer::consumer, ref(pc), 2);

    thd1.join();
    thd2.join();
    thd3.join();
}
