#include <iostream>
#include <future>

using namespace std;

void background_task(int id)
{
    cout << "Start BT#" << id << endl;
    this_thread::sleep_for(chrono::milliseconds(3000));
    cout << "End BT#" << id << endl;
}

int main()
{
    {
        async(launch::async, background_task, 1);
        async(launch::async, background_task, 2);
    }

    cout << "\n-----------------------\n";

    {
        auto f1 = async(launch::async, background_task, 1);
        auto f2 = async(launch::async, background_task, 2);
    }
}
